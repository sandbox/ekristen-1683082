The Splunk Storm module extends the Logging functionality
of Drupal by allowing you to send all watchdog events to 
Splunk's cloud base solution Splunk Storm.

As of July 2012 you can create an account for free at 
http://www.splunkstorm.com and start sending your data
there right away.

To be able to use this module you will require a Splunk
Storm account, a Splunk Storm Project, and an API Access
Token. Once you have all three of those items taken care
of you will be able to configure this module.
